<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class NotesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $formattedNote="It will allow you to preview new attachments with via Drag and \nDrop or Image URL.\n\n(Storing these isn't actually implemented to save storage space)";
        //Purple(#8471FA), Orange(#F46036), Green(#1CB58C), Grey(#2B303A), Red(#E54641)
        $date = Carbon::now()->format('Y-m-d H:i:s');
        DB::table('notes')->insert([
            'title' => 'This is the first test note',
            'content' => 'You can click the trashcan icon to delete me!',
            'colour' => '1CB58C',
            'created_at' => $date,
            'updated_at' => $date
        ]);
        DB::table('notes')->insert([
            'title' => 'Try to edit this note',
            'content' => 'Clicking on a note\'s body will allow you to edit it!',
            'colour' => 'F46036',
            'created_at' => $date,
            'updated_at' => $date
        ]);
        DB::table('notes')->insert([
            'title' => 'You can also change the colour of a note!',
            'content' => 'Click on the palette icon below to swap colours.',
            'colour' => '1CB58C',
            'created_at' => $date,
            'updated_at' => $date
        ]);
        DB::table('notes')->insert([
            'title' => 'Try clicking the + button at the bottom right of your screen',
            'content' => 'It will let you create a new note!',
            'colour' => 'E54641',
            'created_at' => $date,
            'updated_at' => $date
        ]);
        DB::table('notes')->insert([
            'title' => 'Click on the paper clip!',
            'content' => $formattedNote,
            'colour' => '8471FA',
            'created_at' => $date,
            'updated_at' => $date
        ]);
        DB::table('notes')->insert([
            'title' => 'Click the refresh button at the bottom left to restart',
            'content' => 'It will refresh the page content back to the starting state!',
            'colour' => '2B303A',
            'created_at' => $date,
            'updated_at' => $date
        ]);
    }
}