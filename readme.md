

## Michael-MacDonald.com

This site is used for parking small projects and example code.

Each brand is registered a subdomain which holds the project code which applies to it. Try going to "YOUR_BRAND.michael-macdonald.com"

##Pertinent Files
If you aren't familiar with Laravel's directory structure I can point you to some important files:

- /resources
    - This folder houses the SCSS and JS before they are packed.
- /database
    - This folder houses the migrations and database seeder files.
- /app/Http
    - This folder houses the Controllers for all Models. 
- /routes/web.php
    - This file houses all defined Routes.
