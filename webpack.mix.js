const mix = require('laravel-mix');

mix.js('resources/js/Application/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .js('resources/js/Brandboom/brandboom.js', 'public/js')
    .sass('resources/sass/brandboom.scss', 'public/css');
