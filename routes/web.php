<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('home');
//});



Route::group(['middleware' => 'web'], function(){
    Route::domain('brandboom.' . parse_url(config('app.url'), PHP_URL_HOST))->group(function ($router) {
        //Brandboom Home Page
        Route::get('/', function(){ return view('brandboom'); });
    });
    //Load All Notes
    Route::get('/notes', 'NoteController@loadAllNotes');
    //Insert a New Note
    Route::post('/notes', 'NoteController@insertNewNote');
    //Update a Note (All Fields)
    Route::put('/notes/{note}', 'NoteController@updateNote');
    //Update a Note (Colour Only)
    Route::patch('/notes/{note}', 'NoteController@updateColour');
    //Delete a Note
    Route::delete('/notes/{note}', 'NoteController@deleteNote');
    //Delete all Notes and Reseed Table
    Route::delete('/notes', 'NoteController@reseedNotesTable');

    //Returns the Michael Home Page
    Route::get('/', function() { return view('home'); });
});




