<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use NotesTableSeeder;
use App\Note;

class NoteController extends Controller
{
    /**
     * Deletes an existing note
     *
     * @param Note $note
     * @return string
     * @throws \Exception
     */
    public function deleteNote(Note $note){
        $note->delete();
        return "true";
    }

    /**
     * Inserts a new note
     *
     * @param Request $request
     * @return Note $note
     */
    public function insertNewNote(Request $request){
        //In case the JS validation fails or is overridden, this will validate server side as well
        try {
            $this->validate($request, [
                'title' => 'required|min:1|max:100',
                'text' => 'required|min:1|max:255',
                'colour' => 'required',
            ]);
            $note = new Note();
            $note->title = $request->title;
            $note->content = $request->text;
            $note->colour = $request->colour;
            $note->save();

            return $note;
        }
        catch(ValidationException $exception){
            return response()->json([
                'status' => 'error',
                'msg' => 'Error',
                'errors' => $exception->errors(),
            ], 422);
        }
    }

    /**
     * Returns all of the Notes
     *
     * @return mixed
     */
    public function loadAllNotes(){
        return Note::all();
    }

    /**
     * Deletes the current notes and reseeds the Notes Table
     *
     * @return string
     */
    public function reseedNotesTable(){
        Note::truncate(); //Empty table and reset the index
        $seeder = new NotesTableSeeder(); //Create an instance of the Seeder class
        $seeder->run(); //Run the Seeder
        return "true";
    }

    /**
     * Updates the colour of the note only
     *
     * @param Note $note
     * @param Request $request
     * @return Note
     */
    public function updateColour(Note $note, Request $request){
        //In case the JS validation fails or is overridden, this will validate server side as well
        try {
            $this->validate($request, [
                'colour' => 'required',
            ]);
            $note->colour = $request->colour;
            $note->save();

            return $note;
        }
        catch(ValidationException $exception){
            return response()->json([
                'status' => 'error',
                'msg' => 'Error',
                'errors' => $exception->errors(),
            ], 422);
        }

    }

    /**
     * Updates an existing note
     *
     * @param Note $note
     * @param Request $request
     * @return Note $note
     */
    public function updateNote(Note $note, Request $request){
        //In case the JS validation fails or is overridden, this will validate server side as well
        try {
            $this->validate($request, [
                'title' => 'required|min:1|max:100',
                'text' => 'required|min:1|max:255',
                'colour' => 'required',
            ]);
            $note->title = $request->title;
            $note->content = $request->text;
            $note->colour = $request->colour;
            $note->save();

            return $note;
        }
        catch(ValidationException $exception){
            return response()->json([
                'status' => 'error',
                'msg' => 'Error',
                'errors' => $exception->errors(),
            ], 422);
        }

    }
}
