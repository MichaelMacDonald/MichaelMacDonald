<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    //Note Model - id, title (string), content (string), colour  (string, 6), created_at, updated_at

    /**
     * Relationship returns all attachments related to the Note
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany (Attachments)
     */
    public function attachments(){
        return $this->hasMany('App\Attachment');
    }
}
