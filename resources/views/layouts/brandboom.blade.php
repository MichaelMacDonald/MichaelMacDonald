<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ mix('/css/brandboom.css') }}">
    <title>@yield('title')</title>
</head>
<body>
    @yield('body')
    <script src="{{ mix('/js/brandboom.js')}}"></script>
</body>
</html>