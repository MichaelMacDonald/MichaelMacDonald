//Axios - https://www.npmjs.com/package/axios
window.axios = require('axios');
//Set Axios headers
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
//VueJS - https://www.npmjs.com/package/vue
window.Vue = require('vue');

import Home from '../components/Application/Home';

const app = new Vue({
    el: '#app',
    render: h => h(Home)
});