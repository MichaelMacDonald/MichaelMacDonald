//Lodash - https://www.npmjs.com/package/lodash
window._ = require('lodash');
//Axios - https://www.npmjs.com/package/axios
window.axios = require('axios');
//Set Axios headers
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
//VueJS - https://www.npmjs.com/package/vue
window.Vue = require('vue');

//Font Awesome - https://fontawesome.com/
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

//Regular Icon Imports
import { faHome, faPlus, faCheck, faSyncAlt } from '@fortawesome/pro-regular-svg-icons';

//Light Icon Imports

//Solid Icon Imports
import { faTrashAlt, faPaperclip, faPalette } from '@fortawesome/pro-solid-svg-icons';

//Add Selected Icons to Loaded Library (Keeps the size as small as possible by only loading the imported icons)
library.add(faHome, faPlus, faTrashAlt, faPaperclip, faPalette, faCheck, faSyncAlt);

//Register the Font Awesome Icon component
Vue.component('font-awesome-icon', FontAwesomeIcon);
//Register two functions globally for opening and closing modals
Vue.mixin({
    methods: {
        showDialog: function(id){
            let modal = document.getElementById(id);
            modal.style.visibility = 'visible';
            modal.style.opacity = '1';

            let dialog = document.querySelector('#' + id + ' .pop__dialog');
            dialog.style.opacity = '1';
            dialog.style.transform = 'scale(1)';

            document.body.classList.add('no-scroll');
        },
        closeDialog: function(id){
            let dialog = document.querySelector('#' + id + ' .pop__dialog');
            dialog.style.opacity = '0';
            dialog.style.transform = 'scale(.25)';

            let modal = document.getElementById(id);
            modal.style.visibility = 'hidden';
            modal.style.opacity = '0';

            document.body.classList.remove('no-scroll');
        },
    }
})

//Import the Root component
import Brandboom from '../components/Brandboom/Brandboom';

//Render the Root Component
const app = new Vue({
    el: '#brandboom',
    render: b => b(Brandboom)
});